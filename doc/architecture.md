# Architecture

In this document, we will clarify the architecture of our code, how the different parts interact, as well as the choices we have made.

## Model structure
The following figure describes the Database models as well as their relations. This document served as basis for out architecture, and how entities would interact with each other.

![](images/bdd_diagram.jpeg)

## Module interaction

Our app is built of 3 main bricks: 
- The frontend, client-facing app
- The backend, server app
- A PostgreSQL database

The frontend and backend communicate partly using HTTP through a REST API, and mainly using sockets (with Socket.io).

The API and Socket documentation can be found in the [api.md](./api.md) file.

The two following diagrams describe the flow of socket messages sent and received when a user uses the app.

![](images/seq_diagram_1.png)
![](images/seq_diagram_2.png)




