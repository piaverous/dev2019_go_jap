/**
 *
 * JoinJapPage
 *
 */

import React from 'react';
import styled from 'styled-components';
import H1 from 'components/H1';

const Wrapper = styled.div`
  width: 100%;
  margin: 30px;
  display: flex;
  flex-direction: column;
`;

function JoinJapPage() {
  return (
    <Wrapper>
      <H1>Work in progress ...</H1>
    </Wrapper>
  );
}

JoinJapPage.propTypes = {};

export default JoinJapPage;
