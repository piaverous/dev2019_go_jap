import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';
import Wrapper from './Wrapper';

const LoadingIndicator = ({ size }) => (
  <Wrapper>
    <CircularProgress size={size || 40} />
  </Wrapper>
);

LoadingIndicator.propTypes = {
  size: PropTypes.number,
};

export default LoadingIndicator;
